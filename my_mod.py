from numbers import Number


def square(number: Number) -> Number:
    """Return the square of a number.

    >>> square(2)
    4

    >>> square('1')
    Traceback (most recent call last):
    ...
    TypeError: Expected a number, but got a <class 'str'>
    """
    if not isinstance(number, Number):
        raise TypeError(f"Expected a number, but got a {type(number)}")

    return number ** 2


def calculate_difference(num1, num2):
    """Вычисление разницы между двумя числами.    """
    return num1 - num2



if __name__ == "__main__""

    for _ in range(3):
        password = input('Please input your password: ')
        if password == 'qwerty':
            print(f'Access granted. Attempt {1+_} success')
            break
        else:
            print(f'Wrong password, {2-_} tries left')
    else:
        print('Access denied. All 3 attempts failed')



    """код для функции calculate_difference"""
    number1 = float(input("Введите первое число: "))
    number2 = float(input("Введите второе число: "))
    difference = calculate_difference(number1, number2)
    print(f"Разница между {number1} и {number2} равна {difference}")
    """конец кода для функции calculate_difference"""
